import { applyMiddleware, createStore } from 'redux';

import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import { createLogger } from 'redux-logger';

import reducers from './reducers';

const middleware = applyMiddleware(promise(), thunk, createLogger());

const store =  createStore(reducers, middleware);

export default store;
