import {
  FETCH_DATA
} from './constants';
import getAccount from '../lib/api';

export function fetchData() {
  return {
    type: FETCH_DATA,
    payload: getAccount()
  }
}
