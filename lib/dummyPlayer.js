import React from 'react';

const player = {
  account: {
    name: 'Chris',
    email: 'confoxt@gmail.com',
    uid: '0',
  },
  character: {
    id: 'ID0',
    xp: 0,
    stats: {
      strength: 18,
      agility: 10,
      wisdom: 10,
      constitution: 10,
      sense: 10,
      luck: 10,
    },
    statPoints: 0,
    skills: [],
    skillPoints: 0,
    gear: [],
    inventory: [],
    display: {
      name: 'Adventurer',
      gender: 'Male',
      race: 'Human',
      classType: 'Warrior',
      health: 10000,
      maxHealth: 10000,
    },
  },
}

export default player;
