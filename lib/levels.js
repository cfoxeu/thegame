const levels = [
  {
    level: 0,
    enemy: {
      health: 5000,
      strength: 10,
      agility: 15,
      wisdom: 10,
      constitution: 11,
      sense: 12,
      luck: 10,
    },
  },
  {
    level: 1,
    enemy: {
      health: 15000,
      strength: 15,
      agility: 18,
      wisdom: 10,
      constitution: 15,
      sense: 12,
      luck: 10,
    },
  },
]

export default levels;
