import c from '../lib/colorPalette';

const styles = {
  //
  // Elements
  //
  container: { // Full screen
    flex: 1,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
  },
  background: { // Add a background image
    width: null,
    height: null,
    resizeMode: 'cover',
  },
  modal: {
    position: 'absolute',
    width: '80%',
    minHeight: 200,
  },
  loader: {
  },
  combatEntity: {
    alignSelf: 'center',
    height: 80,
    width: 80,
  },
  player: {
    position: 'absolute',
    top: '40%',
    left: 50,
  },
  enemy: {
    position: 'absolute',
    top: '40%',
    right: 50,
  },
  //
  // Text
  //
  title: {
    marginVertical: 16,
    backgroundColor: 'transparent',
    color: c.text.light,
    fontSize: 48,
    fontFamily: 'roughLg',
  },
  welcome: {
    backgroundColor: 'transparent',
    color: c.text.light,
    fontSize: 56,
    fontFamily: 'roughSm',
  },
  textLight: {
    fontSize: 20,
    color: c.text.light,
  },
  textDark: {
    fontSize: 20,
    color: c.text.dark,
  },
  rewards: {
    padding: 20,
  },
  rewardItem: {
    backgroundColor: 'transparent',
    color: c.text.light,
  },
  textShadow: {
    textShadowColor: '#000',
    textShadowOffset: { width: 0, height: 3 },
    textShadowRadius: 3,
  },
  //
  // Buttons
  //
  playBtnContainer: {
    position: 'absolute',
    bottom: 30,
  },
  mainMenuBtnContainer: {
    position: 'absolute',
    top: 5,
    left: 5,
  },
  mainMenuBtn: {
    width: 42,
    height: 42,
  },
  pullOutMenu: {
    position: 'absolute',
    top: 5,
    right: 5,
  },
  pullOutArrow: {
    height: 40,
    width: 40,
    marginBottom: 5,
  },
  pullOutBtn: {
    height: 40,
    width: 40,
    borderRadius: 18,
    borderWidth: 2,
    borderColor: '#eee',
    backgroundColor: c.btn.iconBg,
    marginVertical: 5,
  },
  pullOutIcon: {
    height: 28,
    width: 28,
    margin: 4,
  },
  levelRowStyle: {
    justifyContent: 'center',
    flexDirection: 'row',
  },
  levelIcon: {
    margin: 5,
  },
}

export default styles;
