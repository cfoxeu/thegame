const account = {
  uid: '0',
  username: 'Chris',
  email: 'confoxt@gmail.com', // Should be hashed for security
  name: 'Adventurer',
  gender: 'Male',
  race: 'Human',
  classType: 'Warrior',
  health: 10000,
  maxHealth: 10000, // This is just a base (no gear)
  xp: 0, // Get level from xp
  stats: {
    strength: 18,
    agility: 10,
    wisdom: 10,
    constitution: 10,
    sense: 10,
    luck: 10,
  },
  statPoints: 0,
  skills: [],
  skillPoints: 0,
  inventory: [],
  gear: [],
};

export default () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      return resolve(account)
    }, 500)
  })
}
