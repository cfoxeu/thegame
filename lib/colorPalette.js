import React from 'react';
// Colours
const colors = {
   err: '#f00',
   text: {
     dark: '#112',
     light: '#ffe',
   },
   btn: {
     border: '#a6f',
     bg: '#508',
     innerBg: '#70a',
     text: '#ffd',
     iconBg: '#920',
   },
   neon: {
     red: '#f22',
     blue: '#22f',
     yellow: '#ff2',
     green: '#2f2',
     magenta: '#f2f',
     cyan: '#2ff',
   },
   bg: {
     dark: '#112',
     semiDark: '#213',
     navy: '#227',
     blue: '#339',
     maroon: '#711',
     parchment: '#fdb',
     fade: {
       light: 'rgba(255,255,255,0.1)',
       dark: 'rgba(0,0,0,0.1)',
     },
   },
   hpBar: {
     back: '#111',
     fullHp: '#070',
     medHp: '#f70',
     lowHp: '#d00',
     border: '#ccc',
     dmg: '#fa0',
   },
}

// Others
const awaitingAssignment = [
  '#29f',
  '#8df',
  '#789',
  '#bce',
  '#777',
  '#355',
  '#088',
  '#6aa',
  '#2bb',
  '#080',
  '#bf3',
]

export default colors;
