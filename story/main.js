const story = `
Player backstory

. You wake up in a cryogenic chamber, unsure of why the world has ended/Everyone has
  moved to another planet.
. You are the Son/Daughter of one of the realms main leader, and rebel away.
. Highest Mage/Scout/Warrior sent on a quest to stop the evil/go to the Gods.
  to find out more about what happened.
. You are destined to be the hero to stop evil/restore unity to universe/solar
  system.


Factions/planets

. Each of the 3 classes are from one solar system each solar system has its own
  group of planets.
~ Each planet specialises in one skill from each class (You choose what to
  level up and how to level it up).
. Gods/Immortals - Each god/immortal had its own following/reputation on each
  planet.

Characters

. Scout - 1st of the three classes, the hunter is from a planet that believes
that the power of nature is stronger than what humans can comprehend. He/she
lost their family in a siege against their planet that was performed by the
great relative of an evil and malicious god/by another faction.

. Warrior - The 2nd of the three classes, the Warrior is from a planet that
believes there is a greater more divine power that watches over everyone.
They believe that it is their duty to find a Warrior that will help contain
evil forces or keep them at bay. The Warrior believes it is his/her duty to
be a part of a team that will help them defend the universe from all evil.

Mage - The 3rd of the three classes, the mage is from a planet that believes
that there is a mystic energy that surrounds everyone, but you cannot see it.
They developed skills that allowed them to manipulate this energy and use it
for whatever they intended it for. The mage has been sent on a quest by his/her
master to stop evil forces using the magic energy to do evil and deadly deeds.


1st draft idea

You wake up inside a Cryogenic Chamber. the last thing you remember was that some
sort of apocalypse was happening meteors, demons, cities burning... As you
escape the pod and the building you find a damaged weapon (particular to class
Choice). As you leave the building you come across an unearthly small creature
when you get too close to it it gets aggressive (First battle starts).
You venture on further to find a small village (The last people on earth, this
will be the hub and maybe the outskirts will be the tutorial/first main boss).
The people from the village (Village elder or something cliche) tells you of
these prophecies telling a story of how someone that was once trapped in ice
will one day with the help of two other extraordinary people find the cause of
all the destruction and to put an end to the evil/bring unity between
factions/gods once more. From here you gain knowledge of how to travel through
space. Visiting all the stars and their accompanying planets. Upgrading your
ship to get further and to visit more planets to upgrade more skills.

2nd draft idea

Belonging to the highest order of Mages/Warriors/Scouts, you have been chosen
to go on a quest with two other classes (Whatever you didn't choose) to stop
a great relative of an evil underworld god that is rising up and threatening
to destroy each and every world and populate the galaxy with only his/her own
minions. Your goal is to strengthen yourself in each aspect (each class) and
become more a more powerful team than anyone else (PVP?. Players get a choice
of good or evil.) Going through stages or missions as a team to hopefully defeat
all evil.

`
// Danny this -> ` is a backtick, it holds all your text and makes it a string.
// I have assigned it to the variable story at the top, so that I can export
// what you've written and use it in other places. :)
