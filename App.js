import React, { PureComponent } from 'react';
import {
  AppRegistry,
  Dimensions,
  View,
  Text
} from 'react-native';
import { Provider } from 'react-redux';
import { Font } from 'expo';

import c from './lib/colorPalette';
import s from './lib/styles';
import Layout from './views/pages/Layout';
import store from './redux/store';

class App extends PureComponent {
  state = {
    fontsLoaded: false,
  };
  lockOrientationLandscape() {
    Expo.ScreenOrientation.allow(
      Expo.ScreenOrientation.Orientation.LANDSCAPE
    );
  }
  async componentWillMount() {
    await Font.loadAsync({
      'plainBold': require('./lib/fonts/Roboto-Bold.ttf'),
      'shapes': require('./lib/fonts/Hunt.ttf'),
      'roughSm': require('./lib/fonts/Astonished.ttf'),
      'roughLg': require('./lib/fonts/After_Shok.ttf'),
    });
    this.setState({ fontsLoaded: true });
    this.lockOrientationLandscape();
    const screenWidth = Dimensions.get('window').width;
    const screenHeight = Dimensions.get('window').height;
    console.log('Dimensions: ', screenWidth, 'x', screenHeight);
  }
  render() {
    const { container, loader, textLight } = s;
    return (
      <Provider store={store}>
        <View style={[container, { backgroundColor: c.bg.semiDark }]}>
          {this.state.fontsLoaded
            ? <Layout />
            : <View style={loader}><Text style={textLight}>Loading fonts...</Text></View>
          }
        </View>
      </Provider>
    );
  }
}

export default App;
