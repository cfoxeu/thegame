import React, { PureComponent } from 'react';
import { Image, View, Text } from 'react-native';

import c from '../../lib/colorPalette';
import s from '../../lib/styles';

import BtnLink from '../components/BtnLink';

const rewardsData = [
  {
    'name': 'Gold',
    'qty': 5000,
  },
  {
    'name': 'Test prize',
    'qty': 5,
  },
];

export default class RewardScreen extends PureComponent {
  render() {
    const {
      container,
      background,
      title,
      textShadow,
      rewards,
      rewardItem
    } = s;
    return (
      <Image source={require('../../public/images/space.jpg')} style={[container, background]}>
        <Text style={[title, textShadow]}>Rewards</Text>
        <View style={rewards}>
          {rewardsData.map((reward, i) =>
            <View key={i}>
              <Text style={rewardItem}>{reward.name} x{reward.qty}</Text>
            </View>
          )}
        </View>
        <BtnLink
          title="Ok"
          btnBorder={c.btn.border}
          btnBg={c.btn.bg}
          innerBg={c.btn.innerBg}
          textColor={c.btn.text}
          onPress={this.props.playAdventure}
        />
      </Image>
    );
  }
}
