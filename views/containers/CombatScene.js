import React, { PureComponent } from 'react';
import { StyleSheet, View, Image } from 'react-native';

import c from '../../lib/colorPalette';
import s from '../../lib/styles';

import FightScene from '../components/FightScene';

// Most of this data will eventually be handled with Redux
// For now we're just using dummy data

const eStats = {
  'strength': 10,
  'agility': 15,
  'wisdom': 10,
  'constitution': 11,
  'sense': 12,
  'luck': 10,
};

export default class CombatScene extends PureComponent {
  constructor(props) {
    super(props);
    this.basicAttack = this.basicAttack.bind(this);
    this.state = {
      player: true,
      enemy: true,
      lastAttack: '',
      enemyLastAttack: '',
    };
  }

  componentDidMount() {
    this.setState({ playerHealth: this.props.playerData.maxHealth });
    this.setState({ enemyHealth: this.props.enemy.health });
  }

  componentDidUpdate() {
    if (this.state.enemyHealth < 1) {
      this.setState({ enemy: false });
    }
    if (this.state.playerHealth < 1) {
      this.setState({ player: false });
    }
    if (!this.state.enemy) {
      this.props.rewards();
    }
  }

  hitChance = (minRoll, check) => {
    const hitRoll = Math.floor(Math.random() * (21 - minRoll)) + minRoll;
    if (hitRoll > check) {
      return true
    }
    else {
      return false
    }
  }

  getDamage = (who, health, keyStat, sense, luck, lvl) => {
    const baseDmg = 500;
    const critChance = ((sense + luck) * lvl) / 100;
    const critRoll = Math.floor(Math.random() * 101);
    if (critChance > critRoll) {
      // return true
      const minDmg = (baseDmg + (keyStat + 1) * lvl);
      const maxDmg = (baseDmg + (keyStat + luck) * lvl);
      const normalDmg = Math.floor(Math.random() * (maxDmg - minDmg)) + minDmg;
      const critDmgRoll = Math.floor(Math.random() * (luck - 1)) + 1;
      const critDmg = Math.floor(((keyStat + critDmgRoll) / 100) * normalDmg);
      const damage = normalDmg + critDmg;
      // console.log('Critical Hit!', 'Damage: ', damage);
      const newHealth = health - damage;
      if (who === 'player') {
        this.setState({ enemyHealth: newHealth });
        this.setState({ lastAttack: damage });
      }
      else if (who === 'mob') {
        this.setState({ playerHealth: newHealth });
        this.setState({ enemyLastAttack: damage });
      }
    }
    else {
      // return false
      const minDmg = (baseDmg + (keyStat + 1) * lvl);
      const maxDmg = (baseDmg + (keyStat + luck) * lvl);
      const damage = Math.floor(Math.random() * (maxDmg - minDmg)) + minDmg;
      // console.log('Damage: ', damage);
      const newHealth = health - damage;
      if (who === 'player') {
        this.setState({ enemyHealth: newHealth });
        this.setState({ lastAttack: damage });
      }
      else if (who === 'mob') {
        this.setState({ playerHealth: newHealth });
        this.setState({ enemyLastAttack: damage });
      }
    }
  }

  basicAttack = () => {
    const pStats = this.props.playerData.stats;
    const state = this.state;
    if (state.enemy) {
      // console.log('>>> START (basic attack) HERE <<<');
      if (this.hitChance(pStats.sense, 10)) {
        this.getDamage('player', state.enemyHealth, pStats.strength, pStats.sense, pStats.luck, 8)
      }
      else {
        this.setState({ lastAttack: 'Miss!' });
        // console.log('Player Miss!');
      }
      // console.log('=== END TURN ===');
    }
    if (state.player) {
      // console.log('@@@ BEGIN ENEMY TURN @@@');
      if (this.hitChance(eStats.sense, 10)) {
        this.getDamage('mob', state.playerHealth, eStats.agility, eStats.sense, eStats.luck, 8)
      }
      else {
        this.setState({ enemyLastAttack: 'Miss!' });
        // console.log('Enemy Miss!');
      }
      // console.log('*** END ENEMY TURN ***');
    }
  }

  render() {
    const {
      playerData,
      enemyMaxHealth
    } = this.props;
    const {
      playerHealth,
      lastAttack,
      enemyHealth,
      enemyLastAttack
    } = this.state;
    const {
      container,
      background
    } = s;
    return (
      <Image source={require('../../public/images/town-ruins.jpg')} style={[container, background]}>
        {playerData
          ? <FightScene
              playerMaxHealth={playerData.maxHealth}
              playerHealth={playerHealth}
              lastAttack={lastAttack}
              enemyMaxHealth={enemyMaxHealth}
              enemyHealth={enemyHealth}
              enemyLastAttack={enemyLastAttack}
              basicAttack={this.basicAttack}
            />
          : null
        }
      </Image>
    );
  }
}
