import React, { PureComponent } from 'react';
import { View, Text, Image } from 'react-native';
import c from '../../lib/colorPalette';
import s from '../../lib/styles';

import BtnLink from '../components/BtnLink';

export default class Welcome extends PureComponent {
  render() {
    const props = this.props;
    const {
      container,
      background,
      title,
      welcome,
      textShadow,
      playBtnContainer
    } = s;
    return (
      <Image source={require('../../public/images/sci-fi.jpg')} style={[container, background]}>
        <Text style={[title, textShadow]}>Fragments</Text>
        <Text style={[welcome, textShadow]}>
          Welcome {props.name ? props.name : 'Adventurer'}
        </Text>
        <View style={playBtnContainer}>
          <BtnLink
            title="Play"
            btnBorder={c.btn.border}
            btnBg={c.btn.bg}
            innerBg={c.btn.innerBg}
            textColor={c.btn.text}
            onPress={props.playAdventure}
          />
        </View>
      </Image>
    );
  }
}
