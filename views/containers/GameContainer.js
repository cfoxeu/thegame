import React, { PureComponent } from 'react';
import { View, Text, Image } from 'react-native';

import c from '../../lib/colorPalette';
import s from '../../lib/styles';

import BtnIcon from '../components/BtnIcon';
import LevelMap from '../components/LevelMap';
import CombatScene from './CombatScene';
import PullOutMenu from '../components/PullOutMenu';
import Inventory from '../components/Inventory';
import Stats from '../components/Stats';

export default class GameContainer extends PureComponent {
  constructor() {
    super();
    this.toggleInventory = this.toggleInventory.bind(this);
    this.toggleCharDetails = this.toggleCharDetails.bind(this);
    this.state = {
      inventory: false,
      charDetails: false,
    };
  }
  toggleInventory() {
    this.setState({ inventory: !this.state.inventory });
    console.log('toggle inventory');
  }
  toggleCharDetails() {
    this.setState({ charDetails: !this.state.charDetails });
    console.log('toggle char details');
  }
  render() {
    const props = this.props;
    const {
      container,
      background,
      mainMenuBtnContainer,
      mainMenuBtn
    } = s;
    return (
      <Image source={require('../../public/images/post-apoc.jpg')} style={[container, background]}>
        <LevelMap
          levelSelect={props.battle}
        />
        <View style={mainMenuBtnContainer}>
          <BtnIcon
            btnStyle={mainMenuBtn}
            image={require('../../public/images/back-arrow.png')}
            onPress={props.mainMenu}
          />
        </View>
        <PullOutMenu
          toggleInventory={this.toggleInventory}
          toggleCharDetails={this.toggleCharDetails}
        />
        {this.state.charDetails
          ? <Stats />
          : null
        }
        {this.state.inventory
          ? <Inventory />
          : null
        }
      </Image>
    );
  }
}
