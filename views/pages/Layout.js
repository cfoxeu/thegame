import React, { PureComponent } from 'react';
import {
  View,
  StatusBar
} from 'react-native';
import { connect } from 'react-redux';

import { fetchData } from '../../redux/actions';

import s from '../../lib/styles';

import Router from './Router';

class Layout extends PureComponent {
  render() {
    const { container } = s;
    const {
      fetchData,
      isFetching,
      dataFetched,
      data
    } = this.props;
    return (
      <View style={container}>
        <StatusBar hidden />
        <Router
          fetchData={fetchData}
          isFetching={isFetching}
          dataFetched={dataFetched}
          data={data}
        />
      </View>
    )
  }
}

function mapStateToProps (state) {
  return {
    isFetching: state.isFetching,
    dataFetched: state.dataFetched,
    data: state.data,
  }
}

function mapDispatchToProps (dispatch) {
  return {
    fetchData: () => dispatch(fetchData())
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Layout)
