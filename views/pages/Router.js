import React, { PureComponent } from 'react';
import { View, Text } from 'react-native';

import c from '../../lib/colorPalette';
import s from '../../lib/styles';
import levels from '../../lib/levels';

import Welcome from '../containers/Welcome';
import CombatScene from '../containers/CombatScene';
import GameContainer from '../containers/GameContainer';
import RewardScreen from '../containers/RewardScreen';

export default class Router extends PureComponent {
  constructor(props) {
    super(props);
    this.routeSelector = this.routeSelector.bind(this);
    this.state = {
      screen: '',
    };
  }

  componentWillMount() {
    this.setState({ screen: 'welcome' });
    this.props.fetchData();
  }

  routeSelector = (selected) => {
    this.setState({ screen: selected });
  }

  selectWelcome = () => {
    this.routeSelector('welcome');
  }
  selectAdventure = () => {
    this.routeSelector('adventure');
  }
  selectCombat = () => {
    this.routeSelector('combat');
  }
  rewardScreen = () => {
    this.routeSelector('reward');
  }

  render() {
    const { container, loader, textLight } = s;
    const state = this.state;
    const props = this.props;
    const dataStream = props.data;
    const name = dataStream.name;
    return (
      <View style={container}>
        {props.isFetching
          ? <View style={loader}>
              <Text style={textLight}>Loading account...</Text>
            </View>
          : <View style={container}>
              {(state.screen === 'welcome')
                ? <Welcome
                    name={props.data.name}
                    playAdventure={this.selectAdventure}
                  />
                : null
              }
              {(state.screen === 'adventure')
                ? <GameContainer
                    playerData={dataStream}
                    battle={this.selectCombat}
                    mainMenu={this.selectWelcome}
                  />
                : null
              }
              {(state.screen === 'combat')
                ? <CombatScene
                    level={levels[0]}
                    playerData={dataStream}
                    playAdventure={this.selectAdventure}
                    rewards={this.rewardScreen}
                    enemy={levels[0].enemy}
                    enemyMaxHealth={levels[0].enemy.health}
                  />
                : null
              }
              {(state.screen === 'reward')
                ? <RewardScreen
                    playerData={dataStream}
                    playAdventure={this.selectAdventure}
                  />
                : null
              }
            </View>
        }
      </View>
    );
  }
}
