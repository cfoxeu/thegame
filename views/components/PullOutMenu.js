import React, { PureComponent } from 'react';
import { StyleSheet, View, Text } from 'react-native';

import c from '../../lib/colorPalette';
import s from '../../lib/styles';

import BtnIcon from './BtnIcon';

const Icon = ({ onPress, image, btnStyle, imageStyle }) => (
  <BtnIcon
    btnStyle={btnStyle}
    image={image}
    imageStyle={imageStyle}
    onPress={onPress}
  />
);

const Arrow = ({ onPress, isShown, arrowStyle }) => (
  <BtnIcon
    image={
      isShown
      ? require('../../public/images/menu_arrow_open.png')
      : require('../../public/images/menu_arrow_closed.png')
    }
    imageStyle={arrowStyle}
    onPress={onPress}
  />
);

export default class PullOutMenu extends PureComponent {
  constructor() {
    super();
    this.toggleMenu = this.toggleMenu.bind(this);
    this.state = {
      menuVisible: false,
    };
  }
  toggleMenu() {
    this.setState({ menuVisible: !this.state.menuVisible })
  }
  render() {
    const {
      pullOutMenu,
      pullOutIcons,
      pullOutArrow,
      pullOutBtn,
      pullOutIcon
    } = s;
    return (
      <View style={pullOutMenu}>
        <Arrow
          arrowStyle={pullOutArrow}
          isShown={this.state.menuVisible}
          onPress={this.toggleMenu}
        />
        {this.state.menuVisible
          ? <View>
              <Icon
                btnStyle={pullOutBtn}
                imageStyle={pullOutIcon}
                image={require('../../public/images/bag_icon.png')}
                onPress={this.props.toggleInventory}
              />
              <Icon
                btnStyle={pullOutBtn}
                imageStyle={pullOutIcon}
                image={require('../../public/images/charDetails_icon.png')}
                onPress={this.props.toggleCharDetails}
              />
            </View>
          : null
        }
      </View>
    );
  }
}
