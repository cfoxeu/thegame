import React, { PureComponent } from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

class BtnLink extends PureComponent {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <TouchableOpacity
        style={[
          styles.button,
          {
            backgroundColor: this.props.btnBg,
            borderColor: this.props.btnBorder,
          },
          this.props.extraStyles,
        ]}
        onPress={this.props.onPress}
      >
        <Text
          style={[
            styles.text,
            {
              backgroundColor: this.props.innerBg,
              color: this.props.textColor,
            },
          ]}
        >
          {this.props.title}
        </Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    paddingVertical: 6,
    paddingHorizontal: 8,
    borderWidth: 1,
    borderRadius: 5,
  },
  text: {
    fontSize: 16,
    fontFamily: 'plainBold',
    textAlign: 'center',
    paddingVertical: 5,
    paddingHorizontal: 8,
  },
})

export default BtnLink;
