import React, { PureComponent } from 'react';
import { StyleSheet, View, Text } from 'react-native';

import s from '../../lib/styles';

export default class Stats extends PureComponent {
  render() {
    return (
      <View style={s.modal}>
        <Text style={[s.textLight, {textAlign: 'center'}]}>Stats</Text>
      </View>
    );
  }
}
