import React, { PureComponent } from 'react';
import { StyleSheet, View, Text } from 'react-native';

import s from '../../lib/styles';

export default class Inventory extends PureComponent {
  render() {
    return (
      <View style={s.modal}>
        <Text style={[s.textLight, {textAlign: 'center'}]}>Inventory</Text>
      </View>
    );
  }
}
