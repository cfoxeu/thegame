import React, { PureComponent } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import c from '../../lib/colorPalette';

import HealthBar from '../components/HealthBar';

export default class CombatEntity extends PureComponent {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View>
        <View style={styles.dmg}>
          {this.props.dmgDisplay
            ? <Text style={styles.text}>
                <Text>{this.props.dmgDisplay} </Text>
              </Text>
            : <Text />
          }
        </View>
        <HealthBar
          health={this.props.health}
          maxHealth={this.props.maxHealth}
        />
        <View
          style={[
            styles.entity,
            this.props.isPlayer ? styles.player : styles.enemy
          ]}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  entity: {
    marginHorizontal: 20,
    height: 80,
    width: 40,
  },
  player: {
    backgroundColor: 'green',
  },
  enemy: {
    backgroundColor: 'red',
  },
  dmg: {
    height: 40,
    backgroundColor: 'transparent',
  },
  text: {
    fontFamily: 'roughSm',
    fontSize: 30,
    textAlign: 'center',
    color: c.hpBar.dmg,
  },
})
