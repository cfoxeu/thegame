import React, { PureComponent } from 'react';
import { Image, View, Text } from 'react-native';
import { chunk } from 'lodash';

import c from '../../lib/colorPalette';
import s from '../../lib/styles';
import levels from '../../lib/levels';

import BtnLink from '../components/BtnLink';


export default class Map extends PureComponent {
  constructor(props) {
    super(props);
  }
  render() {
    const {
      container,
      background,
      title,
      textShadow,
      levelIcon,
      levelRowStyle
    } = s;
    return (
      <Image source={require('../../public/images/hangar.jpg')} style={[container, background]}>
        <Text style={[title, textShadow]}>Levels</Text>
        <View>
          {chunk(levels, 8).map((levelRow, i) =>
            <LevelRow
              key={i}
              levelRow={levelRow}
              levelRowStyle={levelRowStyle}
              levelIconStyle={levelIcon}
              onPress={this.props.levelSelect}
            />
          )}
        </View>
      </Image>
    );
  }
}

const LevelRow = ({ levelRow, levelRowStyle, levelIconStyle, onPress }) => (
  <View style={levelRowStyle}>
    {levelRow.map((level, i) =>
      <LevelIcon
        key={i}
        level={level.level}
        levelIconStyle={levelIconStyle}
        onPress={onPress}
      />
    )}
  </View>
);

const LevelIcon = ({ level, levelIconStyle, onPress }) => (
  <BtnLink
    title={level}
    btnBorder={c.btn.border}
    btnBg={c.btn.bg}
    innerBg={c.btn.innerBg}
    textColor={c.btn.text}
    extraStyles={levelIconStyle}
    onPress={onPress}
  />
);
