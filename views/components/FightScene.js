import React, { PureComponent } from 'react';
import { View } from 'react-native';

import c from '../../lib/colorPalette';
import s from '../../lib/styles';

import CombatEntity from './CombatEntity';
import CombatGui from '../components/CombatGui';

class FightScene extends PureComponent {
  render() {
    const {
      enemyLastAttack,
      playerHealth,
      playerMaxHealth,
      basicAttack,
      lastAttack,
      enemyHealth,
      enemyMaxHealth
    } = this.props;
    const {
      container,
      combatEntity,
      player,
      enemy
    } = s;
    return (
      <View style={container}>
        <View style={[combatEntity, player]}>
          <CombatEntity
            isPlayer
            dmgDisplay={enemyLastAttack}
            health={playerHealth}
            maxHealth={playerMaxHealth}
          />
        </View>

        <CombatGui
          basicAttack={basicAttack}
        />

        <View style={[combatEntity, enemy]}>
          <CombatEntity
            dmgDisplay={lastAttack}
            health={enemyHealth}
            maxHealth={enemyMaxHealth}
          />
        </View>
      </View>
    );
  }
};

export default FightScene;
