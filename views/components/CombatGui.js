import React, { PureComponent } from 'react';
import { View } from 'react-native';

import s from '../../lib/styles';

import BtnIcon from './BtnIcon';

export default class CombatGui extends PureComponent {
  render() {
    return (
      <View style={{ position: 'absolute' }}>
        {/* 4 buttons - skills/stances/potions */}
        <BtnIcon
          imageStyle={{ height: 100, width: 100 }}
          image={require('../../public/images/sword_icon.png')}
          onPress={this.props.basicAttack}
        />
        {/* 4 more buttons */}
      </View>
    );
  }
}
