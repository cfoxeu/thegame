import React, { PureComponent } from 'react';
import { TouchableOpacity, Image } from 'react-native';

class BtnIcon extends PureComponent {
  constructor(props) {
    super(props);
  }
  render() {
    const props = this.props;
    return (
      <TouchableOpacity
        style={props.btnStyle}
        onPress={props.onPress}
      >
        <Image
          source={props.image}
          style={props.imageStyle}
        />
      </TouchableOpacity>
    );
  }
}

export default BtnIcon;
