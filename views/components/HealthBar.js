import React, { PureComponent } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import c from '../../lib/colorPalette';

export default class HealthBar extends PureComponent {
  constructor(props) {
    super(props);
    this.checkHealth = this.checkHealth.bind(this);
    this.state = {
      healthBarWidth: 78,
    };
  }
  checkHealth() {
    this.setState({ healthBarWidth: (this.props.health / this.props.maxHealth) * 78 })
  }
  componentDidUpdate() {
    this.checkHealth()
  }
  render() {
    const hpBarWidth = this.state.healthBarWidth;
    // const hpBarColor = { };
    if (hpBarWidth < 80) {
      hpBarColor = { backgroundColor: c.hpBar.fullHp }
    }
    if (hpBarWidth < 40) {
      hpBarColor = { backgroundColor: c.hpBar.medHp }
    }
    if (hpBarWidth < 18) {
      hpBarColor = { backgroundColor: c.hpBar.lowHp }
    }
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.healthBar,
            { width: hpBarWidth },
            hpBarColor,
          ]}
        >
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    borderWidth: 1,
    borderColor: c.hpBar.border,
    backgroundColor: c.hpBar.back,
    width: 80,
    height: 8,
    marginBottom: 16,
  },
  healthBar: {
    position: 'absolute',
    top: 0, left: 0,
    height: 6,
  },
})
